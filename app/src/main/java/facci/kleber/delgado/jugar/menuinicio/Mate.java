package facci.kleber.delgado.jugar.menuinicio;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import facci.kleber.delgado.jugar.R;

public class Mate extends AppCompatActivity {

    Button BTN0, BTN1, BTN2, BTN3, BTN4, BTN5, BTN6, BTN7, BTN8,BTN9,BTN10 ;
    public MediaPlayer va, btn0, btn1, btn2,btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mate);
        BTN0= (Button) findViewById(R.id.BTN0);
        BTN1= (Button) findViewById(R.id.BTN1);
        BTN2= (Button) findViewById(R.id.BTN2);
        BTN3= (Button) findViewById(R.id.BTN3);
        BTN4= (Button) findViewById(R.id.BTN4);
        BTN5= (Button) findViewById(R.id.BTN5);
        BTN6= (Button) findViewById(R.id.BTN6);
        BTN7= (Button) findViewById(R.id.BTN7);
        BTN8= (Button) findViewById(R.id.BTN8);
        BTN9= (Button) findViewById(R.id.BTN9);
        BTN10= (Button) findViewById(R.id.BTN10);

       btn0 = MediaPlayer.create(Mate.this, R.raw.btn0);
        btn1 = MediaPlayer.create(Mate.this, R.raw.btn1);
        btn2 = MediaPlayer.create(Mate.this, R.raw.btn2);
        btn3 = MediaPlayer.create(Mate.this, R.raw.btn3);
        btn4 = MediaPlayer.create(Mate.this, R.raw.btn4);
        btn5 = MediaPlayer.create(Mate.this, R.raw.btn5);
        btn6 = MediaPlayer.create(Mate.this, R.raw.btn6);
        btn7 = MediaPlayer.create(Mate.this, R.raw.btn7);
        btn8 = MediaPlayer.create(Mate.this, R.raw.btn8);
        btn9 = MediaPlayer.create(Mate.this, R.raw.btn9);
        btn10 = MediaPlayer.create(Mate.this, R.raw.btn10);



        //Funcion de boton para el numero 0
        BTN0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn0.start();
                Toast.makeText(getApplicationContext(), "Numero 0", Toast.LENGTH_SHORT).show();

                 }
            });
                //Funcion de boton para el numero 1
        BTN1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn1.start();
                Toast.makeText(getApplicationContext(), "Numero 1", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para el numero 2
        BTN2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn2.start();
                Toast.makeText(getApplicationContext(), "Numeero 2", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para el numero 3
        BTN3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn3.start();
                Toast.makeText(getApplicationContext(), "Numero 3", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para el numero 4
        BTN4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn4.start();
                Toast.makeText(getApplicationContext(), "Numero 4", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para el numero 5
        BTN5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn5.start();
                Toast.makeText(getApplicationContext(), "Numero 5", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para el numero 6
        BTN6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn6.start();
                Toast.makeText(getApplicationContext(), "Numero 6", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para el numero 7
        BTN7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn7.start();
                Toast.makeText(getApplicationContext(), "Numero 7", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para el numero 8
        BTN8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn8.start();
                Toast.makeText(getApplicationContext(), "Numero 8", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para el numero 9
        BTN9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn9.start();
                Toast.makeText(getApplicationContext(), "Numero 9", Toast.LENGTH_SHORT).show();

            }
        });

        //Funcion de boton para el numero 10
        BTN10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn10.start();
                Toast.makeText(getApplicationContext(), "Numero 10", Toast.LENGTH_SHORT).show();

            }
        });
        }
}



