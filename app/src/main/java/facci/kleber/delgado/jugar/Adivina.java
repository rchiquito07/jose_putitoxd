package facci.kleber.delgado.jugar;

import android.app.Activity;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import facci.kleber.delgado.jugar.Adapter.AdapterSpinerCinco;
import facci.kleber.delgado.jugar.Adapter.AdapterSpinnerA;
import facci.kleber.delgado.jugar.Adapter.AdapterSpinnerC;
import facci.kleber.delgado.jugar.Adapter.AdapterSpinnerCuatro;
import facci.kleber.delgado.jugar.Adapter.AdapterSpinnerF;
import facci.kleber.delgado.jugar.Adapter.AdapterSpinnerH;
import facci.kleber.delgado.jugar.Adapter.AdapterSpinnerNueve;
import facci.kleber.delgado.jugar.Items.SpinnerItenCuatro;
import facci.kleber.delgado.jugar.Items.SpinnerItenC;
import facci.kleber.delgado.jugar.Items.SpinnerItenA;
import facci.kleber.delgado.jugar.LogrosAdivinaMayor;
import facci.kleber.delgado.jugar.Items.SpinnerItenCinco;
import facci.kleber.delgado.jugar.Items.SpinnerItenF;
import facci.kleber.delgado.jugar.Items.SpinnerItenNueve;
import facci.kleber.delgado.jugar.Items.SpinnerItenH;



import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Adivina extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout LLunomayor, LLdosmayor, LLtresmayor, LLcuatromayor, LLcincomayor, LLseismayor, LLsietemayor;

    private String SPdomayor = "";
    private String SPremayor = "";
    private String SPmimayor = "";
    private String SPsolmayor = "";
    private String SPlamayor = "";
    private String SPsimayor = "";
    private String SPfamayor = "";
    private String SPfecha;
    private String SPnombre = "";
    private ProgressDialog progressDialog;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference logros_Notas_Mayores, users;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String userId;

    private ArrayList<SpinnerItenC> spinnerItenUnoMayor;
    private AdapterSpinnerC adapterSpinnerUnoMayor;

    private ArrayList<SpinnerItenA> spinnerItenDosMayor;
    private AdapterSpinnerA adapterSpinnerDosMayor;

    private ArrayList<SpinnerItenF> spinnerItenTresMayor;
    private AdapterSpinnerF adapterSpinnerTresMayor;

    private ArrayList<SpinnerItenCuatro> spinnerItenCuatroMayor;
    private AdapterSpinnerCuatro adapterSpinnerCuatroMayor;

    private ArrayList<SpinnerItenCinco> spinnerItenCincoMayor;
    private AdapterSpinerCinco adapterSpinnerCincoMayor;

    private ArrayList<SpinnerItenH> spinnerItenSeisMayor;
    private AdapterSpinnerH adapterSpinnerSeisMayor;

    private ArrayList<SpinnerItenNueve> spinnerItenSieteMayor;
    private AdapterSpinnerNueve adapterSpinnerSieteMayor;


    /* Instanciamos cada objeto de la clase privada Spinner */
    private Spinner spinnerUnoMayor, spinnerDosMayor, spinnerTresMayor, spinnerCuatroMayor,
            spinnerCincoMayor, spinnerSeisMayor, spinnerSieteMayor;

    /* Instanciamos cada objeto de la clase privada Imageview y MediaPlayer */
    private ImageView unoMayor, dosMayor, tresMayor, cuatroMayor, cincoMayor, seisMayor, sieteMayor;
    private MediaPlayer MPunoMayor, MPdosMayor, MPtresMayor, MPcuatroMayor, MPcincoMayor, MPseisMayor, MPsieteMayor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adivina);


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firebaseDatabase = FirebaseDatabase.getInstance();

        /* Obtenemos instancia para autenticar el usuario*/
        firebaseAuth = FirebaseAuth.getInstance();

        /* Obtenemos instancia y referencias del almacenamiento de firebase */
        /* Obetenemos de la base de datos el usuario actual. */
        FirebaseUser user = firebaseAuth.getCurrentUser();
        //espera listo77no hagas nada
        userId = user.getUid();
        //usuario = firebaseDatabase.getReference().child("guitarra-dos/Integrantes");
        usuario = firebaseDatabase.getReference();

        Datos();

        LLunomayor = (LinearLayout) findViewById(R.id.LLUno);
        LLdosmayor = (LinearLayout) findViewById(R.id.LLDos);
        LLtresmayor = (LinearLayout) findViewById(R.id.LLTres);
        LLcuatromayor = (LinearLayout) findViewById(R.id.LLCuatro);
        LLcincomayor = (LinearLayout) findViewById(R.id.LLCinco);
        LLseismayor = (LinearLayout) findViewById(R.id.LLSeis);
        LLsietemayor = (LinearLayout) findViewById(R.id.LLSiete);

        /* A cada referencia de MediaPlayer la enlazamos con su id */
        MPunoMayor = MediaPlayer.create(this, R.raw.btnc);
        MPdosMayor = MediaPlayer.create(this, R.raw.btna);
        MPtresMayor = MediaPlayer.create(this, R.raw.btnf);
        MPcuatroMayor = MediaPlayer.create(this, R.raw.btn4);
        MPcincoMayor = MediaPlayer.create(this, R.raw.btn5);
        MPseisMayor = MediaPlayer.create(this, R.raw.btnh);
        MPsieteMayor = MediaPlayer.create(this, R.raw.btn9);

        /* A cada referencia de las ImageView las enlazamos con su id */
        unoMayor = (ImageView) findViewById(R.id.SPImagenUno);
        dosMayor = (ImageView) findViewById(R.id.SPImagenDos);
        tresMayor = (ImageView) findViewById(R.id.SPImagenTres);
        cuatroMayor = (ImageView) findViewById(R.id.SPImagenCuatro);
        cincoMayor = (ImageView) findViewById(R.id.SPImagenCinco);
        seisMayor = (ImageView) findViewById(R.id.SPImagenSeis);
        sieteMayor = (ImageView) findViewById(R.id.SPImagenSiete);

        //Asociamos el lístener a los botones.
        unoMayor.setOnClickListener(this);
        dosMayor.setOnClickListener(this);
        tresMayor.setOnClickListener(this);
        cuatroMayor.setOnClickListener(this);
        cincoMayor.setOnClickListener(this);
        seisMayor.setOnClickListener(this);
        sieteMayor.setOnClickListener(this);

        LLunomayor.setOnClickListener(this);
        LLdosmayor.setOnClickListener(this);
        LLtresmayor.setOnClickListener(this);
        LLcuatromayor.setOnClickListener(this);
        LLcincomayor.setOnClickListener(this);
        LLseismayor.setOnClickListener(this);


        SpinnerUnoMayorPersonalizado();
        SpinnerDosMayorPersonalizado();
        SpinnerTresMayorPersonalizado();
        SpinnerCuatroMayorPersonalizado();
        SpinnerCincoMayorPersonalizado();
        SpinnerSeisMayorPersonalizado();
        SpinnerSieteMayorPersonalizado();


        /* A cada referencia de los spinner lo enlazamos con su id */
        spinnerUnoMayor = (Spinner) findViewById(R.id.SPUno);
        spinnerDosMayor = (Spinner) findViewById(R.id.SPDos);
        spinnerTresMayor = (Spinner) findViewById(R.id.SPTres);
        spinnerCuatroMayor = (Spinner) findViewById(R.id.SPCuatro);
        spinnerCincoMayor = (Spinner) findViewById(R.id.SPCinco);
        spinnerSeisMayor = (Spinner) findViewById(R.id.SPSeis);
        spinnerSieteMayor = (Spinner) findViewById(R.id.SPSiete);



        progressDialog = new ProgressDialog(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        users = firebaseDatabase.getReference();
        logros_Notas_Mayores = firebaseDatabase.getReference("ADIVINA");
        progressDialog = new ProgressDialog(this);

        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss a");
                                String dateString = sdf.format(date);
                                SPfecha = dateString;
                            }
                        });
                    }
                } catch (InterruptedException e) {

                }
            }
        };
        t.start();




        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerUnoMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenC clickedItem = (SpinnerItenC) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemMayor();
                if (clickedCountryName.equals(getString(R.string.nueve))) {

                    if (SPsimayor == ""){
                        SPsimayor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPsimayor += " - " + "CORRECTO: " + clickedCountryName;
                    }

                    LLunomayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    spinnerUnoMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {
                } else {

                    if (SPsimayor == ""){
                        SPsimayor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPsimayor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLunomayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerDosMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenA clickedItem = (SpinnerItenA) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemDosMayor();
                if (clickedCountryName.equals(getString(R.string.H))) {

                    if (SPlamayor == ""){
                        SPlamayor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPlamayor += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLdosmayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    spinnerDosMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {


                    if (SPlamayor == ""){
                        SPlamayor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPlamayor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLdosmayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerTresMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenF clickedItem = (SpinnerItenF) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemTresMayor();
                if (clickedCountryName.equals(getString(R.string.cinco))) {

                    if (SPfamayor == ""){
                        SPfamayor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPfamayor += " - "+ "CORRECTO: " +clickedCountryName;
                    }
                    LLtresmayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    spinnerTresMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {

                    if (SPfamayor == ""){
                        SPfamayor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPfamayor += " - "+ "INCORRECTO: " +clickedCountryName;
                    }
                    LLtresmayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCuatroMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenCuatro clickedItem = (SpinnerItenCuatro) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCuatroMayor();
                if (clickedCountryName.equals(getString(R.string.cuatro))) {
                    if (SPsolmayor == ""){
                        SPsolmayor = "CORRECTO: " + clickedCountryName;
                    }else{
                        SPsolmayor += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLcuatromayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    spinnerCuatroMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {

                    if (SPsolmayor == ""){
                        SPsolmayor = "INCORRECTO: " + clickedCountryName;
                    }else{
                        SPsolmayor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLcuatromayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCincoMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenCinco clickedItem = (SpinnerItenCinco) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCincoMayor();
                if (clickedCountryName.equals(getString(R.string.F))) {

                    if (SPmimayor == ""){
                        SPmimayor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPmimayor += " - " + "CORRECTO: " +clickedCountryName;
                    }
                    LLcincomayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    spinnerCincoMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {


                    if (SPmimayor == ""){
                        SPmimayor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPmimayor += " - " + "INCORRECTO: " +clickedCountryName;
                    }
                    LLcincomayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSeisMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenH clickedItem = (SpinnerItenH) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSeisMayor();
                if (clickedCountryName.equals(getString(R.string.A))) {

                    if (SPremayor == ""){
                        SPremayor = "CORRECTO: " + clickedCountryName;
                    }else{
                        SPremayor += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLseismayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    spinnerSeisMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {


                    if (SPremayor == ""){
                        SPremayor = "INCORRECTO: " + clickedCountryName;
                    }else{
                        SPremayor += " - " + "INCORRECTO: " + clickedCountryName;
                    }
                    LLseismayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSieteMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenNueve clickedItem = (SpinnerItenNueve) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSieteMayor();
                if (clickedCountryName.equals(getString(R.string.C))) {

                    if (SPdomayor == ""){
                        SPdomayor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPdomayor += " - " + "CORRECTO: " +clickedCountryName ;
                    }
                    LLsietemayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    spinnerSieteMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {

                    if (SPdomayor == ""){
                        SPdomayor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPdomayor += " - " + "INCORRECTO: " +clickedCountryName ;
                    }

                    LLsietemayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* A cada refrencia de los spinner lo enlazamos con su id */
        spinnerUnoMayor = (Spinner) findViewById(R.id.SPUno);
        spinnerDosMayor = (Spinner)findViewById(R.id.SPDos);
        spinnerTresMayor = (Spinner)findViewById(R.id.SPTres);
        spinnerCuatroMayor = (Spinner)findViewById(R.id.SPCuatro);
        spinnerCincoMayor = (Spinner)findViewById(R.id.SPCinco);
        spinnerSeisMayor = (Spinner)findViewById(R.id.SPSeis);
        spinnerSieteMayor = (Spinner)findViewById(R.id.SPSiete);

        /* Asignados el origen de los datos desde los recursos. */
        adapterSpinnerUnoMayor = new AdapterSpinnerC(this, spinnerItenUnoMayor);
        adapterSpinnerDosMayor = new AdapterSpinnerA(this, spinnerItenDosMayor);
        adapterSpinnerTresMayor = new AdapterSpinnerF(this, spinnerItenTresMayor);
        adapterSpinnerCuatroMayor = new AdapterSpinnerCuatro(this, spinnerItenCuatroMayor);
        adapterSpinnerCincoMayor = new AdapterSpinerCinco(this, spinnerItenCincoMayor);
        adapterSpinnerSeisMayor = new AdapterSpinnerH(this, spinnerItenSeisMayor);
        adapterSpinnerSieteMayor = new AdapterSpinnerNueve(this, spinnerItenSieteMayor);

        /* Seteamos el adaptador */
        spinnerUnoMayor.setAdapter(adapterSpinnerUnoMayor);
        spinnerDosMayor.setAdapter(adapterSpinnerDosMayor);
        spinnerTresMayor.setAdapter(adapterSpinnerTresMayor);
        spinnerCuatroMayor.setAdapter(adapterSpinnerCuatroMayor);
        spinnerCincoMayor.setAdapter(adapterSpinnerCincoMayor);
        spinnerSeisMayor.setAdapter(adapterSpinnerSeisMayor);
        spinnerSieteMayor.setAdapter(adapterSpinnerSieteMayor);
    }

    private void Datos(){

        usuario.child("Integrantes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Usuario usuario_perfil = snapshot.getValue(Usuario.class);
                    String key = usuario.child("Integrantes").child(userId).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)){
                        SPnombre = usuario_perfil.getNombres();
                    }else {
                        Log.e("Nombres: ", usuario_perfil.getNombres());
                    }

                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    /* Creamos un arreglo para el primer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerUnoMayorPersonalizado () {
        spinnerItenUnoMayor = new ArrayList<>();
        spinnerItenUnoMayor.add(new SpinnerItenC(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenC(getString(R.string.C), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenC(getString(R.string.H), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenC(getString(R.string.cinco), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenC(getString(R.string.A), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenC(getString(R.string.nueve), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenC(getString(R.string.F), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenC(getString(R.string.cuatro), R.drawable.ic_nota_dp));

    }

    /* Creamos un arreglo para el segundo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerDosMayorPersonalizado () {
        spinnerItenDosMayor = new ArrayList<>();
        spinnerItenDosMayor.add(new SpinnerItenA(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenA(getString(R.string.A), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenA(getString(R.string.cinco), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenA(getString(R.string.C), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenA(getString(R.string.F), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenA(getString(R.string.cuatro), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenA(getString(R.string.nueve), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenA(getString(R.string.H), R.drawable.ic_nota_dp));


    }


    /* Creamos un arreglo para el tercer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerTresMayorPersonalizado () {
        spinnerItenTresMayor = new ArrayList<>();
        spinnerItenTresMayor.add(new SpinnerItenF(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenF(getString(R.string.cuatro), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenF(getString(R.string.cinco), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenF(getString(R.string.F), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenF(getString(R.string.A), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenF(getString(R.string.C), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenF(getString(R.string.nueve), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenF(getString(R.string.H), R.drawable.ic_nota_dp));


    }

    /* Creamos un arreglo para el cuarto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCuatroMayorPersonalizado () {
        spinnerItenCuatroMayor = new ArrayList<>();
        spinnerItenCuatroMayor.add(new SpinnerItenCuatro(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatro(getString(R.string.cuatro), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatro(getString(R.string.C), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatro(getString(R.string.H), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatro(getString(R.string.cinco), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatro(getString(R.string.nueve), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatro(getString(R.string.F), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatro(getString(R.string.A), R.drawable.ic_nota_dp));


    }

    /* Creamos un arreglo para el quinto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCincoMayorPersonalizado () {
        spinnerItenCincoMayor = new ArrayList<>();
        spinnerItenCincoMayor.add(new SpinnerItenCinco(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCinco(getString(R.string.nueve), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCinco(getString(R.string.H), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCinco(getString(R.string.cuatro), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCinco(getString(R.string.cinco), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCinco(getString(R.string.F), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCinco(getString(R.string.A), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCinco(getString(R.string.C), R.drawable.ic_nota_dp));


    }

    /* Creamos un arreglo para el sexto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSeisMayorPersonalizado () {
        spinnerItenSeisMayor = new ArrayList<>();
        spinnerItenSeisMayor.add(new SpinnerItenH(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenH(getString(R.string.C), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenH(getString(R.string.A), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenH(getString(R.string.F), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenH(getString(R.string.cinco), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenH(getString(R.string.cuatro), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenH(getString(R.string.H), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenH(getString(R.string.nueve), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el septimo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSieteMayorPersonalizado () {
        spinnerItenSieteMayor = new ArrayList<>();
        spinnerItenSieteMayor.add(new SpinnerItenNueve(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenNueve(getString(R.string.C), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenNueve(getString(R.string.F), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenNueve(getString(R.string.A), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenNueve(getString(R.string.cinco), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenNueve(getString(R.string.H), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenNueve(getString(R.string.cuatro), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenNueve(getString(R.string.nueve), R.drawable.ic_nota_dp));


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            /* Se reproduce el sonido del primer botón */
            case R.id.SPImagenUno:
                MPsieteMayor.start();
                break;

            /* Se reproduce el sonido del segundo botón */
            case R.id.SPImagenDos:
                MPseisMayor.start();
                break;

            /* Se reproduce el sonido del tercer botón */
            case R.id.SPImagenTres:
                MPcincoMayor.start();
                break;

            /* Se reproduce el sonido del cuarto botón */
            case R.id.SPImagenCuatro:
                MPcuatroMayor.start();
                break;

            /* Se reproduce el sonido del quinto botón */
            case R.id.SPImagenCinco:
                MPtresMayor.start();
                break;

            /* Se reproduce el sonido del sexto botón */
            case R.id.SPImagenSeis:
                MPdosMayor.start();
                break;

            /* Se reproduce el sonido del septimo botón */
            case R.id.SPImagenSiete:
                MPunoMayor.start();
                break;

        }
    }


    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){

            if (SPdomayor== "" || SPremayor=="" || SPmimayor=="" || SPfamayor=="" || SPsolmayor=="" ||SPlamayor=="" || SPsimayor==""){
                Toast.makeText(this, "FALTAN ITEM POR ELEGIR", Toast.LENGTH_SHORT).show();
            }else {

                isOnlineNet();

                if (isOnlineNet() == true) {
                    progressDialog.setTitle("GUARDANDO DATOS");
                    progressDialog.setMessage("ESPERE POR FAVOR..!");
                    progressDialog.show();

                    /* Creamos objetos de la clases logros para poder hacer uso de esa clase */
                    LogrosAdivinaMayor logros = new LogrosAdivinaMayor();

                    logros.setEscribedomayor7(SPdomayor);
                    logros.setEscriberemayor7(SPremayor);
                    logros.setEscribemimayor7(SPmimayor);
                    logros.setEscribefamayor7(SPfamayor);
                    logros.setEscribesolmayor7(SPsolmayor);
                    logros.setEscribelamayor7(SPlamayor);
                    logros.setEscribesimayor7(SPsimayor);
                    logros.setFecha(SPfecha);
                    logros.setNombre7(SPnombre);


                    logros_Notas_Mayores.child(FirebaseAuth.getInstance().getUid()).setValue(logros).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            progressDialog.hide();
                            startActivity(new Intent(Adivina.this, Main2Activity.class));
                            finish();

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });

                    //}else {

                    //}
                } else {
                    Toast.makeText(this, "DATOS NO GUARDADOS, SIN ACCESO A INTERNE", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Adivina.this, Main2Activity.class));
                    finish();
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){

    }

    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {

            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

}