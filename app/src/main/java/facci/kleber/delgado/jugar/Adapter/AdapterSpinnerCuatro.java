package facci.kleber.delgado.jugar.Adapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import facci.kleber.delgado.jugar.Items.SpinnerItenCuatro;
import facci.kleber.delgado.jugar.R;
import java.util.ArrayList;

public class AdapterSpinnerCuatro extends ArrayAdapter<SpinnerItenCuatro> {


    public AdapterSpinnerCuatro(Context context, ArrayList<SpinnerItenCuatro> NumberList) {
        super(context, 0, NumberList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_uno, parent, false
            );
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.IMGSPUnoMenor);
        TextView textView = (TextView) convertView.findViewById(R.id.LBLSPUnomenor);

        SpinnerItenCuatro spinnerIten = getItem(position);

        if (spinnerIten != null) {
            imageView.setImageResource(spinnerIten.getImagenItemCuatroMayor());
            textView.setText(spinnerIten.getNombreItemCuatroMayor());
        }

        return convertView;
    }
}

