package facci.kleber.delgado.jugar.Modelo;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import facci.kleber.delgado.jugar.LogrosAdivinaMayor;
import facci.kleber.delgado.jugar.R;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AdivinaLogrosMayores extends Fragment {

    private ListView listView;
    private FirebaseListAdapter adapter;
//seguro no hay datos pos
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adivina_logros_mayores, container, false);
        listView = (ListView) view.findViewById(R.id.LVMostrarAdivinasNotasMayoresFragmento);
        final DatabaseReference query = FirebaseDatabase.getInstance().getReference().child("ADIVINA_SEPTIMA_MAYORES");
        final FirebaseListOptions<LogrosAdivinaMayor> logrosMayoresFirebaseListOptions = new FirebaseListOptions.Builder<LogrosAdivinaMayor>()
                .setLayout(R.layout.logros_adivina_7_mayor)
                .setQuery(query,LogrosAdivinaMayor.class).build();
        adapter = new FirebaseListAdapter(logrosMayoresFirebaseListOptions) {

            @Override
            protected void populateView(View v, Object model, int position) {

                TextInputEditText doMayor7 = (TextInputEditText) v.findViewById(R.id.DoMenor7A);
                TextInputEditText reMayor7 = (TextInputEditText) v.findViewById(R.id.ReMenor7A);
                TextInputEditText miMayor7 = (TextInputEditText) v.findViewById(R.id.MiMenor7A);
                TextInputEditText faMayor7 = (TextInputEditText) v.findViewById(R.id.FaMenor7A);
                TextInputEditText solMayor7 = (TextInputEditText) v.findViewById(R.id.SolMenor7A);
                TextInputEditText laMayor7= (TextInputEditText) v.findViewById(R.id.LaMenor7A);
                TextInputEditText siMayor7 = (TextInputEditText) v.findViewById(R.id.SiMenor7A);
                TextInputEditText fechaMayor7 = (TextInputEditText) v.findViewById(R.id.FechaMayor7);
                TextInputEditText nombresUMayor7 = (TextInputEditText) v.findViewById(R.id.NombreUser7);

                LogrosAdivinaMayor logrosAdivinaMayor = (LogrosAdivinaMayor) model;
                doMayor7.setText(logrosAdivinaMayor.getEscribedomayor7());
                reMayor7.setText(logrosAdivinaMayor.getEscriberemayor7());
                miMayor7.setText(logrosAdivinaMayor.getEscribemimayor7());
                faMayor7.setText(logrosAdivinaMayor.getEscribefamayor7());
                solMayor7.setText(logrosAdivinaMayor.getEscribesolmayor7());
                laMayor7.setText(logrosAdivinaMayor.getEscribelamayor7());
                siMayor7.setText(logrosAdivinaMayor.getEscribesimayor7());
                fechaMayor7.setText(logrosAdivinaMayor.getFecha());
                nombresUMayor7.setText(logrosAdivinaMayor.getNombre7());


            }
        };

        listView.setAdapter(adapter);
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();

    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}

