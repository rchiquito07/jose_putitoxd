package facci.kleber.delgado.jugar.Items;

public class SpinnerItenF
// SpinnerItenTres
{
    private String NombreItemTresMayor;
    private int ImagenItemTresMayor;

    public String getNombreItemTresMayor() {
        return NombreItemTresMayor;
    }

    public int getImagenItemTresMayor() {
        return ImagenItemTresMayor;
    }
    public SpinnerItenF(String NombreItemTresMayor, int ImagenItemTresMayor) {
        this.NombreItemTresMayor = NombreItemTresMayor;
        this.ImagenItemTresMayor = ImagenItemTresMayor;
    }
}
